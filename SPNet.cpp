#include "SPNet.h"
#include <iomanip>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;


// Default constructor: Random key, min# of rounds = 4
SPNet::SPNet(int nr)
{
    // Make sure number of rounds >= 4
    if (nr < 4)
    {
        numRounds = 4;
    } else
    {
        numRounds = nr;
    }

    // Random key generated
    cout << "--------------- RANDOM KEY: ----------------------" << endl;
    srand(time(NULL));
    key = new Byte[KEY_LEN];

    for (int i = 0; i < KEY_LEN; i++)
    {
        key[i] = (Byte) rand() % KEY_RANGE;
    }
    //PrintArray(key, KEY_LEN);
    //cout << endl;

    // Generate Subkeys
    //cout << "--------------- GENERATED SUBKEYS: ---------------" << endl;
    GenerateSubkeys();

    // Generate Permutation Matrix
    cout << "--------------- GENERATED PERMUTATION: -----------" << endl;
    GeneratePermutationMatrix();

    cout << "--------------------------------------------------" << endl;

    // Generate S Matrix
    cout << "--------------- GENERATED S: ---------------------" << endl;
    GenerateSMatrix();
}

/*
// Destructor
// Delete all data members that are dynamically allocated of the SPNet object

*/
SPNet::~SPNet()
{
// Destroy key
    delete[] key;

// Destroy subkeys
    for (int i = 0; i < numRounds; ++i)
    {
        delete[] subkeys[i];
    }
    delete[] subkeys;
}

// Key schedule: A simple function for the key schedule is that for subkey of round r, subkey K_r is a copy of the original key starting from byte 3i + 1, wrapped around if necessary. This is not a secure way to generate key in practice. It's good to demonstrate linear cryptanalysis, however.
void SPNet::GenerateSubkeys()
{
    subkeys = new Byte *[numRounds + 1];
    for (int i = 0; i < numRounds + 1; ++i)
    {
        subkeys[i] = new Byte[BLOCK_LEN];
        for (int j = 0; j < BLOCK_LEN; j++)
        {
            subkeys[i][j] = key[(j + (3 * i + 1)) % KEY_LEN];
        }
        //PrintArray(subkeys[i], BLOCK_LEN);
        //cout << endl;
    }
}

void SPNet::S(const Byte *input, Byte substituted[])
{
    for (int i = 0; i < BLOCK_LEN; i++)
    {
        substituted[i] = sMatrix[i][input[i]];
    }
}


void SPNet::Sinv(const Byte *input, Byte *substituted)
{
    for (int i = 0; i < BLOCK_LEN; i++)
    {
        // Find the index of input[i]
        for (int j = 0; j < 256; j++)
        {
            if (input[i] == sMatrix[i][j])
            {
                substituted[i] = j;
            }
        }
    }
}

void SPNet::P(const Byte *input, Byte permuted[], bool encrypt)
{
    // Do the permutation as a matrix-vector multiplication
    int sum;
    for (int i = 0; i < BLOCK_LEN; i++)
    {
        sum = 0;
        if (encrypt)
        {
            for (int j = 0; j < BLOCK_LEN; j++)
            {
                sum = sum + ((int) input[j]) * pMatrix[i][j];
            }
        } else
        {
            for (int j = 0; j < BLOCK_LEN; j++)
            {
                sum = sum + ((int) input[j]) * pMatrixInverse[i][j];
            }
        }
        permuted[i] = (Byte) sum;
    }
}

// XOR operation
void SPNet::XOR(const Byte *input, Byte XORed[],
                          int numSubkey)
{
    for (int i = 0; i < BLOCK_LEN; i++)
    {
        XORed[i] = input[i] ^ subkeys[numSubkey][i];
    }
}

/***************************************************
 * ENCRYPTION
 ***************************************************
 * Encrypt a string plaintext
 */
Byte *SPNet::Encrypt(const Byte *plaintext, int len)
{
    int currIndex = 0;

    // Prepare input string
    auto numSubInput = (int) (len / BLOCK_LEN);
    if (len % BLOCK_LEN != 0) { numSubInput++; }
//	Byte input[numSubInput][BLOCK_LEN]; // somehow this doesn't work in testing image. possibly because the image is too big
    Byte **input;
    input = new Byte *[numSubInput];
    for (int i = 0; i < numSubInput; i++)
    {
        input[i] = new Byte[BLOCK_LEN];
    }

    PrepareString(plaintext, input, len);
    Byte *ciphertext = new Byte[numSubInput * BLOCK_LEN];

    for (int s = 0; s < numSubInput; s++)
    {
        Byte *tmp = SPNEncrypt(input[s]);

        // Convert the result back to type string and copy to ciphertext string
        for (int i = 0; i < BLOCK_LEN; i++)
        {
            ciphertext[currIndex] = tmp[i];
            currIndex++;
        }

        delete[] tmp;
    }

    for (int i = 0; i < numSubInput; i++)
    {
        delete[] input[i];
    }
    delete[] input;

    return ciphertext;
}

// Encrypt Algorithm
Byte *SPNet::SPNEncrypt(const Byte *in)
{
    Byte *ciphertext = new Byte[BLOCK_LEN];

    // SPNet MAIN ALGORITHM
    Byte *XORed, *substituted, *permuted;
    XORed = new Byte[BLOCK_LEN];
    substituted = new Byte[BLOCK_LEN];
    permuted = new Byte[BLOCK_LEN];

    // copy subinput input[s] to permuted as pre-round
    for (int i = 0; i < BLOCK_LEN; i++)
    {
        permuted[i] = in[i];
    }

    // run through the encryption rounds
    for (int r = 0; r < numRounds - 1; r++)
    {
        // XOR result of last round with corresponding subkey of current round
        XOR(permuted, XORed, r);

        // Substitution Pi_S()
        S(XORed, substituted);

        // Permutation Pi_P()
        P(substituted, permuted, PERMUTATION_ENCRYPT_MODE);
    }
    // the last round does not permute the result, only XOR and pi_S()
    XOR(permuted, XORed, numRounds - 1);
    S(XORed, substituted);

    // Output whitening using the last subkey. Recall that we produce
    // (numRounds + 1) subkeys. The first (numRounds) subkeys have been used.
    XOR(substituted, ciphertext, numRounds);

    // destroy intermediate step's materials
    delete[] XORed;
    delete[] substituted;
    delete[] permuted;

    return ciphertext;
}

/***************************************************
 * DECRYPTION
 ***************************************************
 * Decrypt an array of encrypted ciphertext characters in hexa form
 */
Byte *SPNet::Decrypt(const Byte *ciphertext, int len)
{
    int currIndex = 0;

    // Prepare input string
    auto numSubInput = (int) (len / BLOCK_LEN);
    if (len % BLOCK_LEN != 0) { numSubInput++; }
//	Byte input[numSubInput][BLOCK_LEN]; // somehow this doesn't work in testing image. possibly because the image is too big
    Byte **input;
    input = new Byte *[numSubInput];
    for (int i = 0; i < numSubInput; i++)
    {
        input[i] = new Byte[BLOCK_LEN];
    }

    PrepareString(ciphertext, input, len);
    Byte *plainText = new Byte[numSubInput * BLOCK_LEN];

    for (int s = 0; s < numSubInput; s++)
    {
        Byte *tmp = SPNDecrypt(input[s]);

        // Convert the result back to type string and copy to ciphertext string
        for (int i = 0; i < BLOCK_LEN; i++)
        {
            plainText[currIndex] = (char) ((int) tmp[i]);
            currIndex++;
        }

        delete[] tmp;
    }

    for (int i = 0; i < numSubInput; i++)
    {
        delete[] input[i];
    }
    delete[] input;

    return plainText;
}

// Decryption Algorithm
Byte *SPNet::SPNDecrypt(const Byte *in)
{
    Byte *plaintext = new Byte[BLOCK_LEN];

    Byte *XORed, *substituted, *permuted;
    XORed = new Byte[BLOCK_LEN];
    substituted = new Byte[BLOCK_LEN];
    permuted = new Byte[BLOCK_LEN];

    // copy subinput input[s] to permuted as pre-round
    for (int i = 0; i < BLOCK_LEN; i++)
    {
        permuted[i] = in[i];
    }

    // De-whitening
    XOR(permuted, XORed, numRounds);

    // Unwind the last pi_S() and XOR
    Sinv(XORed, substituted);
    XOR(substituted, XORed, numRounds - 1);

    // run through the decryption rounds
    for (int r = numRounds - 2; r > -1; r--)
    {
        // Unwind Permutation Pi_P()
        P(XORed, permuted, PERMUTATION_DECRYPT_MODE); // bool encrypt is false

        // Unwind Substitution Pi_S()
        Sinv(permuted, substituted);

        // Unwind XOR of last round with corresponding subkey of current round
        XOR(substituted, XORed, r);
    }

    for (int i = 0; i < BLOCK_LEN; i++)
    {
        plaintext[i] = XORed[i];
    }

    // destroy intermediate step's materials
    delete[] XORed;
    delete[] substituted;
    delete[] permuted;

    return plaintext;
}


//**************************************************
// Permutation matrix generator for pi_P()
//**************************************************
void SPNet::GeneratePermutationMatrix()
{
    srand(time(NULL));
    int row = rand() % BLOCK_LEN;
    bool flag[BLOCK_LEN] = {false}; // flag to know what columns already have a 1

    for (int i = 0; i < BLOCK_LEN; i++)
    {
        for (int j = 0; j < BLOCK_LEN; j++)
        {
            pMatrix[i][j] = 0;
            pMatrixInverse[i][j] = 0;
        }
    }

    // the <row>th entry of each column is set to 1
    // such that it's the only 1 on that row
    for (int i = 0; i < BLOCK_LEN; i++)
    {
        row = rand() % BLOCK_LEN;
        // if there's already a 1 on the current row, keep running rand()
        while (flag[row] == true)
        {
            row = rand() % BLOCK_LEN;
        }
        flag[row] = true;
        pMatrix[row][i] = 1;
        pMatrixInverse[i][row] = 1; // transpose(pMatrix) = inverse(pMatrix)
    }

    /*cout << "Permutation Matrix (for Encryption): " << endl;
    for (int i = 0; i < BLOCK_LEN; i++)
    {
        for (int j = 0; j < BLOCK_LEN; j++)
        {
            cout << pMatrix[i][j] << "  ";
        }
        cout << endl;
    }
    cout << endl;

    cout << "Inverse Permutation Matrix (for Decryption): " << endl;
    for (int i = 0; i < BLOCK_LEN; i++)
    {
        for (int j = 0; j < BLOCK_LEN; j++)
        {
            cout << pMatrixInverse[i][j] << "  ";
        }
        cout << endl;
    }*/
}

//**************************************************
// Input processor: Turn array input into a 2D array of BLOCK_LEN sub-arrays
//**************************************************
void SPNet::PrepareString(const Byte *input, Byte **in, int len)
{
    int row = 0, currIndex = 0;

    // Process string input into Byte array for encryption;
    for (int i = 0; i < len; i++)
    {
        if (i % BLOCK_LEN == 0) { currIndex = 0; }
        else { currIndex++; }
        row = (int) i / BLOCK_LEN;
        in[row][currIndex] = input[i];
    }
    // Pad the last substring with 0's if needed
    if (len % BLOCK_LEN != 0)
    {
        for (int i = len % BLOCK_LEN; i < BLOCK_LEN; i++)
        {
            in[(int) len / BLOCK_LEN][i] = (Byte) 0;
        }
    }
}
// print an Byte array as hexadecimal values
void SPNet::PrintArray(const Byte *in, int len)
{
    for (int i = 0; i < len; i++)
    {
        cout << hex << setw(4) << (int) in[i];
    }
}

void SPNet::GenerateSMatrix()
{
    srand(time(NULL));
    std::random_device rd;
    std::mt19937 g(rd());

    for (int i = 0; i < BLOCK_LEN; i++)
    {
        for (int j = 0; j < 256; j++)
        {
            sMatrix[i][j] = 0;
        }
    }

    for (int i = 0; i < BLOCK_LEN; i++)
    {
        std::vector<int> shuffle_vec(256);

        for (int j = 0; j < 256; j++)
        {
            shuffle_vec[j] = j;
        }

        std::shuffle(shuffle_vec.begin(), shuffle_vec.end(), g);

        for (int j = 0; j < 256; j++)
        {
            sMatrix[i][j] = shuffle_vec[j];
        }
    }
}

