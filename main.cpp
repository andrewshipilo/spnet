#include <iostream>
#include <QtWidgets/QApplication>
#include "SPNet.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/types_c.h"
#include <qcustomplot.h>

void AutoCorrelation(const unsigned char *encrypted, int cols, int rows);

using namespace cv;
using std::cout;
using std::endl;
using std::string;
using std::cin;

void Plot(std::vector<double> &x, std::vector<double> &y, QCustomPlot *plot, QColor color, int plotNum,
                           const std::string &plotName)
{
    QVector<double> X = QVector<double>::fromStdVector(x);
    QVector<double> Y = QVector<double>::fromStdVector(y);

    plot->resize(512, 512);
    plot->legend->setVisible(true);
    plot->legend->setFont(QFont("Helvetica", 9));
    plot->addGraph(plot->yAxis, plot->xAxis);
    plot->graph(plotNum)->setName(QString::fromStdString(plotName));
    plot->graph(plotNum)->setPen(QPen(color));
    plot->graph(plotNum)->setLineStyle(QCPGraph::lsNone);
    plot->graph(plotNum)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    plot->graph(plotNum)->setData(X, Y, true);
    plot->xAxis->setRange(0, 255);
    plot->yAxis->setRange(0, 255);

    plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

    plot->show();
}

void PlotGraph(std::vector<double> &x, std::vector<double> &y, QCustomPlot *plot, QColor color, int plotNum,
          const std::string &plotName)
{
    QVector<double> X = QVector<double>::fromStdVector(x);
    QVector<double> Y = QVector<double>::fromStdVector(y);

    plot->resize(512, 512);
    plot->legend->setVisible(true);
    plot->legend->setFont(QFont("Helvetica", 9));
    plot->addGraph(plot->xAxis, plot->yAxis);
    plot->graph(plotNum)->setName(QString::fromStdString(plotName));
    plot->graph(plotNum)->setPen(QPen(color));
    plot->graph(plotNum)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    plot->graph(plotNum)->setData(X, Y);


    plot->rescaleAxes();
    plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

    plot->show();
}

void Test(const Byte* input, long len)
{

    std::vector<double> x, y;

    for (int i = 0; i < len - 1; i++)
    {
        x.push_back(input[i]);
    }
    for (int i = 1; i < len; i++)
    {
        y.push_back(input[i]);
    }

    auto plot = new QCustomPlot();

    Plot(x, y, plot, Qt::blue, 0, "Test 1");
}

double Correlation(const Byte* source, const Byte* encoded, long width, long height, long minus = 0)
{
    long dataSize = width * height - minus;
    double ev1 = 0;
    double ev2 = 0; // Expected value

    for (int i = 0; i < dataSize; i++)
    {
        ev1 += source[i];
        ev2 += encoded[i];
    }
    ev1 /= dataSize;
    ev2 /= dataSize;

    double evRR = 0, evGG = 0, evBB = 0;
    double sd1R = 0, sd1G = 0, sd1B = 0;
    double sd2R = 0, sd2G = 0, sd2B = 0; // Standard deviation

    for (int i = 0; i < dataSize; i++)
    {
        double R1 = source[i] - ev1;

        double R2 = encoded[i] - ev2;

        evRR += R1 * R2;

        sd1R += R1 * R1;

        sd2R += R2 * R2;
    }

    evRR /= dataSize;

    sd1R = std::sqrt(sd1R / (width * (height - 1) - minus));
    sd2R = std::sqrt(sd2R / (width * (height - 1) - minus));

    // Calculate correlation coeff
    double corrRR = evRR / (sd1R * sd2R);

    std::cout << "Correlation between source and encrypted images: " << corrRR << std::endl;

    return corrRR;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SPNet newSPN(1);
    string filename = "lena.bmp";


    Mat plain = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);

    if (!plain.data)
    {
        cout << "ERROR: Can't load image." << endl;
        return 1;
    }


    int len = plain.rows * plain.cols;
    auto tmp_plain = new Byte[len];
    auto tmp_encrypted = new Byte[len];

    int currIndex = 0;

    for (int i = 0; i < plain.rows; i++)
    {
        for (int j = 0; j < plain.cols; j++)
        {
            tmp_plain[currIndex] = plain.at<Byte>(i, j);
            currIndex++;
        }
    }
    //Test(tmp_plain, len);
    //Correlation(tmp_plain, tmp_plain, plain.cols, plain.rows);
    AutoCorrelation(tmp_plain, plain.cols, plain.rows);
    tmp_encrypted = newSPN.Encrypt(tmp_plain, len);

    //Test(tmp_encrypted, len);
    //Correlation(tmp_plain, tmp_encrypted, plain.cols, plain.rows);

    AutoCorrelation(tmp_encrypted, plain.cols, plain.rows);
    /*for (int i = 0; i < plain.rows; i++)
    {
        long loc = i * plain.cols;
        auto encrypted = newSPN.Encrypt(tmp_plain + i * plain.cols, plain.cols);
        for (int j = 0; j < plain.cols; j++)
        {
            tmp_encrypted[loc + j] = encrypted[j];
        }
    }

    for (int i = 0; i < plain.cols; i++)
    {
        auto column = new Byte[plain.rows];

        for (int j = 0; j < plain.rows; j++)
        {
            column[j] = tmp_encrypted[j * plain.rows + i];
        }

        auto encrypted = newSPN.Encrypt(column, plain.rows);

        for (int j = 0; j < plain.rows; j++)
        {
            tmp_encrypted[j * plain.rows + i] = encrypted[j];
        }

    }
*/
    Mat encrypted;
    encrypted.create(plain.rows , plain.cols, plain.type());void Plot(std::vector<double> &x, std::vector<double> &y, QCustomPlot *plot, QColor color, int plotNum,
                           const std::string &plotName)
{
    QVector<double> X = QVector<double>::fromStdVector(x);
    QVector<double> Y = QVector<double>::fromStdVector(y);

    plot->resize(512, 512);
    plot->legend->setVisible(true);
    plot->legend->setFont(QFont("Helvetica", 9));
    plot->addGraph(plot->yAxis, plot->xAxis);
    plot->graph(plotNum)->setName(QString::fromStdString(plotName));
    plot->graph(plotNum)->setPen(QPen(color));
    plot->graph(plotNum)->setLineStyle(QCPGraph::lsNone);
    plot->graph(plotNum)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    plot->graph(plotNum)->setData(X, Y, true);
    plot->xAxis->setRange(0, 255);
    plot->yAxis->setRange(0, 255);

    plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

    plot->show();
}

    for (int i = 0; i < plain.rows; i++)
    {
        for (int j = 0; j < plain.cols; j++)
        {
            encrypted.at<Byte>(i, j) = tmp_encrypted[i * plain.cols + j];
        }
    }


    string resultFile(filename);
    resultFile += "_result.jpg";
    imwrite(resultFile, encrypted);


    auto tmp_decrypted = new Byte[len];

    tmp_decrypted = newSPN.Decrypt(tmp_encrypted, len);

    Mat decrypted;
    decrypted.create(plain.rows , plain.cols, plain.type());

    for (int i = 0; i < plain.rows; i++)
    {
        for (int j = 0; j < plain.cols; j++)
        {
            decrypted.at<Byte>(i, j) = tmp_decrypted[i * plain.cols + j];
        }
    }

    string decryptedFile(filename);
    decryptedFile += "_decrypted.jpg";
    imwrite(decryptedFile, decrypted);

    a.exec();

    delete[] tmp_encrypted;
    delete[] tmp_plain;

    return 0;
}

void AutoCorrelation(const unsigned char *encrypted, int cols, int rows)
{
    std::vector<double> x;
    std::vector<double> y;

    for (int i = 0; i < 250; i += 5)
    {
        Byte* first = new Byte[cols * rows];
        Byte* second = new Byte[cols * rows];

        for (int j = 0; j < cols * rows - i; j++)
        {
            first[j] = encrypted[j];
        }
        for (int j = i; j < cols * rows; j++)
        {
            second[j - i] = encrypted[j];
        }

        x.push_back(i);

        std::cout << "R = " << i << std::endl;
        auto corr = Correlation(first, second, cols, rows, i);
        y.push_back(corr);

        delete []first;
        delete []second;
    }

    auto plot = new QCustomPlot();

    PlotGraph(x, y, plot, Qt::blue, 0, "Autocorrelation");
}
