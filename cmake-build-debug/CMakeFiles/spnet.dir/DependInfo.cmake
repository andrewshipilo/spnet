# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/andrew/Git/spnet/SPNet.cpp" "/home/andrew/Git/spnet/cmake-build-debug/CMakeFiles/spnet.dir/SPNet.cpp.o"
  "/home/andrew/Git/spnet/main.cpp" "/home/andrew/Git/spnet/cmake-build-debug/CMakeFiles/spnet.dir/main.cpp.o"
  "/home/andrew/Git/spnet/qcustomplot.cpp" "/home/andrew/Git/spnet/cmake-build-debug/CMakeFiles/spnet.dir/qcustomplot.cpp.o"
  "/home/andrew/Git/spnet/cmake-build-debug/spnet_automoc.cpp" "/home/andrew/Git/spnet/cmake-build-debug/CMakeFiles/spnet.dir/spnet_automoc.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_MULTIMEDIA_LIB"
  "QT_NETWORK_LIB"
  "QT_PRINTSUPPORT_LIB"
  "QT_WIDGETS_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "/usr/include/opencv"
  "/home/andrew/Qt/5.5/gcc/include"
  "/home/andrew/Qt/5.5/gcc/include/QtWidgets"
  "/home/andrew/Qt/5.5/gcc/include/QtGui"
  "/home/andrew/Qt/5.5/gcc/include/QtCore"
  "/home/andrew/Qt/5.5/gcc/./mkspecs/linux-g++-32"
  "/home/andrew/Qt/5.5/gcc/include/QtMultimedia"
  "/home/andrew/Qt/5.5/gcc/include/QtNetwork"
  "/home/andrew/Qt/5.5/gcc/include/QtPrintSupport"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
