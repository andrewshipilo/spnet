#ifndef SPNET_SPNET_H
#define SPNET_SPNET_H

#define Byte unsigned char
#define KEY_LEN 256
#define KEY_RANGE 256
#define BLOCK_LEN 60
#define PERMUTATION_ENCRYPT_MODE true
#define PERMUTATION_DECRYPT_MODE false

class SPNet
{

public:

    // Default constructor: Random key, min# of rounds = 4
    explicit SPNet(int nr = 4);

    // Destructor
    ~SPNet();

    // Encryption for a string input
    Byte* Encrypt(const Byte *plaintext, int len);

    // Decryption for an array of ciphertext characters
    Byte* Decrypt(const Byte *ciphertext, int len);

    // print an unsigned char array as hexadecimal values
    void PrintArray(const Byte *in, int len);

    // Input processor: Turn string input into a 2D array of BLOCK_LEN substrings
    void PrepareString(const Byte *input,
                       Byte **in, int len);

private:

    int numRounds;
    Byte* key; // default length = KEY_LEN
    Byte** subkeys; // there are (numRounds + 1) subkeys of length KEY_LEN
    int pMatrix[BLOCK_LEN][BLOCK_LEN]; // matrix for P()
    int sMatrix[BLOCK_LEN][256];
    int pMatrixInverse[BLOCK_LEN][BLOCK_LEN]; // inverse matrix of P()

    // Key schedule: populate 2-D array subkeys from key
    void GenerateSubkeys();

    // XOR operation with key materials
    void XOR(const Byte *input, Byte *XORed,
             int numSubkey);

    // Substitution S()
    void S(const Byte *input, Byte *substituted);

    void Sinv(const Byte *input, Byte *substituted);

    // Permutation P()
    void P(const Byte *input, Byte *permuted, bool encrypt);

    // Permutation matrix generator for P()
    void GeneratePermutationMatrix();

    // Encrypt Algorithm
    unsigned char* SPNEncrypt(const Byte *in);

    // Decrypt Algorithm
    unsigned char* SPNDecrypt(const Byte *in);

    void GenerateSMatrix();
};


#endif
